//QUERY OPERATORS
//=========================Comparison Query Opearators
//  -----------------$gt(>)/$gte(>=) 

//SEARCH age > 50
db.users.find({
	age: {
		$gt:50
	}
})
//SEARCH age > 50
db.users.find({
	age: {
		$gte:50
	}
})


//------------------$lt(<)/$lte(<=) 
db.users.find({
	age: {
		$lt:50
	}
})

//-------------------NOT EQUAL ($ne)
db.users.find({
	age: {
		$ne: 82
	}
})

//FIND USERS l.name either "Hawking" or "Doe" 
//----------------------($in) 
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})
db.users.find({
	courses: {
		$in: ["HTML", "React"] // limited in one fields
	}
})

//=========================LOGICAL QUERY OPERATORS
//-------------------($or) operators

db.users.find({
	$or: [
		{lastName:{$in: ["Hawking", "Doe"]}}, // can use more fields
		{age: 21}
	]
})

//------------------($and)

db.users.find({
	$and: [
		{age:{$ne: 82}}, // can use more fields
		{age:{$ne: 76}}
	]
})






// =========================== Inclusion
//- Allow us to include/add specific
// if zero value its excluded.

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 0,
	lastName: 0,
	contact:0
}
)

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact:1
}
)

// =========== EXCLUSION: department & contact (VALUE = 0)
db.users.find(
{
	firstName: "Jane"
},
{
	department: 0,
	contact:0
}
)

//========SUpressing the ID field COMBINATION OF EXCLUSION AND INCLUSION
/*-ALlow us to exclude the "_id" field when retrieving documents
-WHen using field projection, field eclusion*/

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1, 		//included
	lastName: 1, 		//included
	contact:1, 			//included
	_id:0				//excluded
}
)

// =================returning specific  fields in embedded document
db.users.find(
{firstName: "Jane"},
{
	firstName: 1,
	lastName: 1,
	"contact.phone":1 // <========== phone is embedded to contact
}
)

db.users.find(
{firstName: "Jane"},
{
	"contact.phone":0 
}
).pretty();



//========================================($slice)
//Project specific array elements int the returned array

db.users.find(
{ namearr:{firstName: "juan"}
},
{ namearr: 
		{$slice:1}
}
)

db.inventory.find( { }, 
{ qty: 1, "details.colors": { $slice: 1} 
} )


db.users.find(

{ "namearr":{name: "Jane"}
},
{ "namearr": 
		{$slice:1}
}
)


//CORECCT SLICES
db.users.find(
{ firstName: "Jane"},
{ courses: { $slice: 1}}
)


//FIND ALL
db.users.find()

//=============$regex operator
// -allows us to find documents that a specific string pattern using regular expression.
// -syntax:

db.users.find({field: redex: 'pattern', $options: '$optionValue'});


db.users.find({firstName: {$regex:"N"}}).pretty();
db.users.find({firstName: {$regex:"j", $options:'$i'}}); // case in-sensitive

db.users.find({
	$or: [
	{firstname: {$regex: 's', $options:'$i'}},
	{lastName: {$regex: 'd', $options:'$i'}}
	]
},
{firstName: 1, lastName: 1, _id: 0}
)




















































